package fi.tamk.tiko.myspaceshooter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Atte Huhtakangas on 20.1.2015 12:19.
 * -
 * Part of myspaceshooter in package fi.tamk.tiko.myspaceshooter.
 */
public class Spaceship extends GameObject {
    private static final String TAG = Spaceship.class.getName();
    private ParticleEffect effect;
    private float rotation;
    private int lives;
    private final float MOVESPEED = 400.0f;

    public Spaceship(int viewportWidth, int viewportHeight) {
        super(AssetLoader.instance.spaceship.spaceship, viewportWidth, viewportHeight);

        // Create new Particle Effect and start it
        effect = new ParticleEffect(AssetLoader.instance.spaceship.exhaust);
        effect.setPosition(rectangle.x + rectangle.getWidth() / 2, rectangle.y + rectangle.getHeight() / 2);
        effect.start();
    }

    @Override
    public void init() {
        super.init();
        rotation = 0;
        lives = 5;
    }

    public int getLives() {
        return lives;
    }

    @Override
    public void die() {
        if (lives > 0) {
            Gdx.app.log(getTag(), "took damage");
            Gdx.app.log(getTag(), "lives: " + lives);
            lives--;
        } else {
            super.die();
        }

    }

    @Override
    protected Rectangle setRectangle() {
        return new Rectangle(MathUtils.random(reg.getRegionWidth(), viewportWidth - reg.getRegionHeight() * 2), MathUtils.random(reg.getRegionWidth(),
                viewportHeight - reg.getRegionHeight() * 2), reg.getRegionWidth(), reg.getRegionHeight());
    }

    public void move(float deltaTime) {
        rectangle.x -= MOVESPEED * Math.sin(Math.toRadians(rotation)) * deltaTime;
        rectangle.y += MOVESPEED * Math.cos(Math.toRadians(rotation)) * deltaTime;

        // Update rectangle x
        if (rectangle.x + rectangle.getWidth() >= viewportWidth) {
            rectangle.x = viewportWidth - rectangle.getWidth() - 2;
        } else if (rectangle.x <= 0) {
            rectangle.x = 2;
        }

        // Update rectangle y
        if (rectangle.y + rectangle.getHeight() >= viewportHeight) {
            rectangle.y = viewportHeight - rectangle.getHeight() - 2;
        } else if (rectangle.y <= 0) {
            rectangle.y = 2;
        }

        // Set particle effects position
        effect.setPosition(rectangle.x + rectangle.getWidth() / 2, rectangle.y + rectangle.getHeight() / 2);
    }

    public boolean collidesWith(Alien alien) {
        if (rectangle.overlaps(alien.getRectangle()))
            return true;
        return false;
    }

    @Override
    public String getTag() {
        return TAG;
    }

    public void draw(SpriteBatch batch) {
        batch.draw(reg.getTexture(),                            // texture
                rectangle.x, rectangle.y,                       // x, y
                rectangle.getWidth() / 2, rectangle.getHeight() / 2,               // originX, originY
                rectangle.getWidth(), rectangle.getHeight(),    // width, height
                scale.x, scale.y,                               // scaleX, scaleY
                rotation,                                       // rotation
                reg.getRegionX(), reg.getRegionY(),             // srcX, srcY
                reg.getRegionWidth(), reg.getRegionHeight(),    // srcWidth, srcHeight
                false, false                                    // flipX, flipY
        );
    }

    public void drawEffect(SpriteBatch batch, float deltaTime) {
        float a = rotation - 90.0f;
        Array<ParticleEmitter> emitters = effect.getEmitters();

        for (int i = 0; i < emitters.size; i++) {
            ParticleEmitter.ScaledNumericValue val = emitters.get(i).getAngle();
            float angleSpread = (effect.getEmitters().get(i).getAngle().getHighMin() - effect.getEmitters().get(i).getAngle().getHighMax()) / 2;
            val.setHigh(a + angleSpread, a - angleSpread);
            val.setLow(a);
        }

        effect.draw(batch, deltaTime);
    }

    public void rotateRight(float deltaTime) {
        rotate(-200.0f, deltaTime);
    }

    public void rotateLeft(float deltaTime) {
        rotate(200.0f, deltaTime);

    }

    public void rotate(float speed, float deltaTime) {
        rotation += speed * deltaTime;
        if (rotation >= 360.0f)
            rotation = 0;
        if (rotation < 0)
            rotation = 360.0f;
    }

    public Bullet shoot() {
        return new Bullet(rectangle.x + rectangle.getWidth() / 2,
                rectangle.y + rectangle.getHeight() / 2, rotation,
                viewportWidth, viewportHeight);
    }
}
