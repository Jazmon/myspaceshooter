package fi.tamk.tiko.myspaceshooter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Atte Huhtakangas on 20.1.2015 13:47.
 * -
 * Part of myspaceshooter in package fi.tamk.tiko.myspaceshooter.
 */
public class Bullet extends GameObject {
    @Override
    public String getTag() {
        return TAG;
    }

    private static final String TAG = Bullet.class.getName();

    private float rotation;
    private final float MOVESPEED = 500.0f;
    private ParticleEffect effect;

    public Bullet(float x, float y, float rotation, int viewportWidth, int viewportHeight) {
        //super("bullet.png", viewportWidth, viewportHeight);
        super(AssetLoader.instance.bullet.bullet, viewportWidth, viewportHeight);
        //texture = new Texture("bullet.png");
        //position.set(x, y);
        rectangle = new Rectangle(x - reg.getRegionWidth() / 2, y - reg.getRegionHeight() / 2, reg.getRegionWidth(), reg.getRegionHeight());
        scale = new Vector2(1.0f, 1.0f);
        this.rotation = rotation;

        effect = new ParticleEffect(AssetLoader.instance.bullet.trail);
        //effect = AssetLoader.instance.bullet.trail;
        //effect.load(Gdx.files.internal("effects/lazor.p"), Gdx.files.internal("effects"));
        effect.setPosition(rectangle.x + rectangle.getWidth() / 2, rectangle.y + rectangle.getHeight() / 2);

        float a = this.rotation - 90.0f;
        Array<ParticleEmitter> emitters = effect.getEmitters();
        for (int i = 0; i < emitters.size; i++) {
            ParticleEmitter.ScaledNumericValue val = emitters.get(i).getAngle();
            float angleSpread = (effect.getEmitters().get(i).getAngle().getHighMin() - effect.getEmitters().get(i).getAngle().getHighMax()) / 2;
            val.setHigh(a + angleSpread, a - angleSpread);
            val.setLow(a);
        }

        effect.start();
        Gdx.app.debug(TAG, "created");
    }

    public void move(float deltaTime) {
        rectangle.x -= MOVESPEED * Math.sin(Math.toRadians(rotation)) * deltaTime;
        rectangle.y += MOVESPEED * Math.cos(Math.toRadians(rotation)) * deltaTime;
        effect.setPosition(rectangle.x + rectangle.getWidth() / 2, rectangle.y + rectangle.getHeight() / 2);
    }

    public void draw(SpriteBatch batch) {
        batch.draw(reg.getTexture(),                            // texture
                rectangle.x, rectangle.y,                       // x, y
                rectangle.getWidth() / 2, rectangle.getHeight() / 2,                       // originX, originY
                rectangle.getWidth(), rectangle.getHeight(),    // width, height
                scale.x, scale.y,                               // scaleX, scaleY
                rotation,                                       // rotation
                reg.getRegionX(), reg.getRegionY(),             // srcX, srcY
                reg.getRegionWidth(), reg.getRegionHeight(),    // srcWidth, srcHeight
                false, false                                    // flipX, flipY
        );
    }

    public void drawEffect(SpriteBatch batch, float deltaTime) {
        effect.draw(batch, deltaTime);
    }

}
