package fi.tamk.tiko.myspaceshooter;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;

public class MySpaceShooter implements ApplicationListener {
    private static final String TAG = MySpaceShooter.class.getName();
    private SpriteBatch batch;
    private Texture background;
    private BitmapFont font;
    private long previousShotTime;
    private long previousHurtSoundTime;
    private OrthographicCamera camera;
    private OrthographicCamera guiCamera;
    private boolean paused;
    private boolean desktop;
    private boolean accelerometerPresent;
    private int aliensOnScreen;
    private int score;
    private int alienSpawnCount;
    private long startTime;

    // Game objects
    private Spaceship spaceship;
    private List<Alien> aliens;
    private List<Bullet> bullets;

    // Audio files
    private Music music;
    private Sound crash;
    private Sound laserShot;
    private Sound explosion;

    public void create() {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        Gdx.app.debug(TAG, "created");

        AssetLoader.instance.init(new AssetManager());
        desktop = false;
        paused = false;
        accelerometerPresent = false;

        font = new BitmapFont();
        font.setColor(Color.WHITE);
        font.setScale(2.0f);

        // Set up the camera
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
        batch = new SpriteBatch();

        // Set up the GUI camera
        guiCamera = new OrthographicCamera();
        guiCamera.setToOrtho(false, Constants.GUI_WIDTH, Constants.GUI_HEIGHT);

        // Load background texture
        background = AssetLoader.instance.levelDecoration.background;

        // Load audio files
        music = AssetLoader.instance.music.music1;
        crash = AssetLoader.instance.sounds.crash;
        laserShot = AssetLoader.instance.sounds.laserShot;
        explosion = AssetLoader.instance.sounds.explosion;

        // Start the music
        music.setLooping(true);
        music.play();

        // Create game objects and arrays
        spaceship = new Spaceship(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
        aliens = new ArrayList<Alien>();
        bullets = new ArrayList<Bullet>();

        // Set up variables to try to get the appropriate input type
        if (Gdx.app.getType() == Application.ApplicationType.Desktop) {
            desktop = true;
        } else if (Gdx.app.getType() == Application.ApplicationType.Android) {
            desktop = false;
            if (Gdx.input.isPeripheralAvailable(Input.Peripheral.Accelerometer)) {
                accelerometerPresent = true;
            }
        } else {
            Gdx.app.error(TAG, "Unknown device type!");
        }

        init();
    }

    public void init() {
        alienSpawnCount = 5;
        previousShotTime = 0;
        score = 0;
        aliensOnScreen = 0;

        aliens.clear();
        bullets.clear();
        spaceship.init();


        // Spawn the initial aliens
        spawnMoreAliens(true);

        startTime = TimeUtils.millis();
    }

    @Override
    public void resize(int width, int height) {
        Gdx.app.debug(TAG, "window resized");
    }

    @Override
    public void render() {
        float deltaTime = Gdx.graphics.getDeltaTime();

        long elapsedTime = TimeUtils.millis() - startTime;

        // Just resetting the score here if it's below 0..
        if(score < 0) score = 0;

        // If game is over we just draw the score screen
        if(!spaceship.isAlive()) {
            Gdx.gl.glClearColor(39 / 255.0f, 169 / 255.0f, 225 / 255.0f, 0xff / 255.0f);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            batch.begin();
            // draw background
            batch.draw(background, 0, 0);
            font.draw(batch,"GAME OVER!", Constants.GUI_WIDTH / 2 - 70, Constants.GUI_HEIGHT / 2);
            font.draw(batch,"SCORE: " + score, Constants.GUI_WIDTH / 2 - 70, Constants.GUI_HEIGHT / 2 - 35);
            batch.end();

            // check if screen pressed and restart game
            if(Gdx.input.isTouched()) {
                init();
            }

            return;

        }

        // Pause the game when started to minimize the amount of bugs etc.
        if(elapsedTime < 3000) {
            CharSequence timeRem = "Starting in: " + String.valueOf( 2 - (int)(elapsedTime / 1000));
            renderWorld(batch, deltaTime);
            renderGUI(batch);
            batch.begin();
            font.draw(batch, timeRem,Constants.GUI_WIDTH / 2 - 70, Constants.GUI_HEIGHT / 2);
            batch.end();
            return;
        }

        // If game not paused get input and update world
        if (!paused) {

            // Check user input and move player
            if (desktop)
                checkUserInput(deltaTime);
            else
                checkUserInputAndroid(deltaTime);
            // Just move the ship forward all the time.
            spaceship.move(deltaTime);
            // Move game objects
            moveObjects(deltaTime);
            // Check for collisions
            checkCollision();
            // Remove dead objects
            removeDead();
            // Spawn more aliens if 0 alive
            spawnMoreAliens(true);
        }

        // Do the actual rendering
        renderWorld(batch, deltaTime);
        renderGUI(batch);
    }

    private void renderWorld(SpriteBatch batch, float deltaTime) {
        batch.setProjectionMatrix(camera.combined);

        Gdx.gl.glClearColor(39 / 255.0f, 169 / 255.0f, 225 / 255.0f, 0xff / 255.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        // draw background
        batch.draw(background, 0, 0);
        // draw bullets
        drawBullets(batch, deltaTime);
        // draw effect
        spaceship.drawEffect(batch, deltaTime);
        // draw player's spaceship
        spaceship.draw(batch);
        // draw aliens
        drawAliens(batch);
        batch.end();
    }

    private void renderGUI(SpriteBatch batch) {
        batch.setProjectionMatrix(guiCamera.combined);
        batch.begin();

        font.draw(batch, "FPS: " + Gdx.graphics.getFramesPerSecond(),8, 30);
        font.draw(batch, aliensOnScreen + " aliens", Constants.GUI_WIDTH - 150, Constants.GUI_HEIGHT - 12);
        font.draw(batch, "SCORE: " + score, Constants.GUI_WIDTH - 200, 30);
        font.draw(batch, "LIVES: " + spaceship.getLives(), 5, Constants.GUI_HEIGHT - 12);
        batch.end();
    }

    private void checkUserInputAndroid(float deltaTime) {
        if (!accelerometerPresent) {
            Gdx.app.error(TAG, "No accelerometer present!");
            return;
        }

        float accelY = Gdx.input.getAccelerometerY();
        float rotateSpeed = -150.0f * accelY;

        // if phone tilted right along y-axis
        if (accelY >= 1) {
            spaceship.rotate(rotateSpeed, deltaTime);
        }
        // rotated left
        else if (accelY <= -1) {
            spaceship.rotate(rotateSpeed, deltaTime);
        }

        if (Gdx.input.isTouched()) {
            /*int touchX = Gdx.input.getX();
            if (touchX <= Constants.VIEWPORT_WIDTH / 4) {
                spaceship.move(deltaTime);
            }*/

            if (TimeUtils.timeSinceMillis(previousShotTime) >= 170) {
                previousShotTime = TimeUtils.millis();
                bullets.add(spaceship.shoot());
                laserShot.play(0.5f);
            }
        }
    }

    private void checkUserInput(float deltaTime) {
        if (Gdx.input.isKeyPressed(Input.Keys.DPAD_RIGHT)) {
            spaceship.rotateRight(deltaTime);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DPAD_LEFT)) {
            spaceship.rotateLeft(deltaTime);
        }
        /*if (Gdx.input.isKeyPressed(Input.Keys.DPAD_UP)) {
            spaceship.move(deltaTime);
        }*/
        // Spawn more aliens just for lols
        if (Gdx.input.isKeyPressed(Input.Keys.T)) {
            spawnMoreAliens(false);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            if (TimeUtils.timeSinceMillis(previousShotTime) >= 150) {
                previousShotTime = TimeUtils.millis();
                bullets.add(spaceship.shoot());
                laserShot.play(0.5f);
            }
        }
        if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE)){
            Gdx.app.exit();
        }
    }

    private void spawnMoreAliens(boolean spawnOnlyIfZeroAlive) {
        int aliveCount = 0;

        if (spawnOnlyIfZeroAlive) {
            for (Alien alien : aliens)
                if (alien.isAlive())
                    aliveCount++;
        }

        if (aliveCount <= 1) {
            for(int i = 0; i < alienSpawnCount; i++) {
                aliens.add(new Alien(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT));
                increaseAlienCount();
            }

            alienSpawnCount++;
        }
    }

    private void moveObjects(float deltaTime) {
        moveAliens(deltaTime);
        moveBullets(deltaTime);
    }

    private void removeDead() {
        // Remove dead aliens
        for (int i = 0; i < aliens.size(); i++) {
            if (!aliens.get(i).isAlive()) {
                aliens.get(i).dispose();
                aliens.remove(i);
            }
        }

        // Remove dead bullets
        for (int i = 0; i < bullets.size(); i++) {
            if (!bullets.get(i).isAlive()) {
                bullets.get(i).dispose();
                bullets.remove(i);
            }
        }
    }

    private void drawBullets(SpriteBatch batch, float deltaTime) {
        for (Bullet bullet : bullets) {
            if (bullet.isAlive()) {
                bullet.draw(batch);
                bullet.drawEffect(batch, deltaTime);
            }
        }
    }

    private void moveBullets(float deltaTime) {
        for (Bullet bullet : bullets)
            bullet.move(deltaTime);
    }

    private void drawAliens(SpriteBatch batch) {
        for (Alien alien : aliens) {
            if (alien.isAlive())
                alien.draw(batch);
        }
    }

    public void increaseAlienCount() {
        aliensOnScreen++;
    }

    public void decreaseAlienCount() {
        aliensOnScreen--;
    }

    private void moveAliens(float deltaTime) {
        for (Alien alien : aliens)
            alien.move(deltaTime);
    }

    private void checkCollision() {
        // Check if spaceship collides with any alien
        for (Alien alien : aliens) {
            if (spaceship.collidesWith(alien)) {
                if (TimeUtils.timeSinceMillis(previousHurtSoundTime) >= 400) {
                    previousHurtSoundTime = TimeUtils.millis();
                    crash.play(0.6f);
                    score = score - 5;
                    spaceship.die();
                }
            }

            // Check if any bullet collides with any alien
            for (Bullet bullet : bullets) {
                if (bullet.collidesWith(alien)) {
                    bullet.die();
                    explosion.play(0.6f);
                    decreaseAlienCount();
                    score = score + 10;
                    alien.die();
                }
            }
        }

        // Check edge collisions
        for (Bullet bullet : bullets) {
            // check collision
            if (bullet.getRectangle().x + bullet.getRectangle().width >= Constants.VIEWPORT_WIDTH) {
                bullet.die();
            } else if (bullet.getRectangle().x <= 0) {
                bullet.die();
            }
            if (bullet.getRectangle().y + bullet.getRectangle().height >= Constants.VIEWPORT_HEIGHT) {
                bullet.die();
            } else if (bullet.getRectangle().y <= 0) {
                bullet.die();
            }
        }
    }

    @Override
    public void pause() {
        Gdx.app.debug(TAG, "paused");
        paused = true;
    }

    @Override
    public void resume() {
        Gdx.app.debug(TAG, "resumed");
        paused = false;
    }

    @Override
    public void dispose() {
        Gdx.app.debug(TAG, "dispose() called");
        AssetLoader.instance.dispose();
        spaceship.dispose();
        music.dispose();
        background.dispose();
        crash.dispose();
        laserShot.dispose();
        removeAliens();
        removeBullets();
        batch.dispose();
        font.dispose();
    }

    private void removeBullets() {
        for (Bullet bullet : bullets) {
            bullet.dispose();
        }
        for (int i = 0; i < bullets.size(); i++) {
            bullets.remove(i);
        }
    }

    private void removeAliens() {
        for (Alien alien : aliens) {
            alien.dispose();
        }
        for (int i = 0; i < aliens.size(); i++) {
            aliens.remove(i);
        }
    }
}
