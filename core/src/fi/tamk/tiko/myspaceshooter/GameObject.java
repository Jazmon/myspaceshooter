package fi.tamk.tiko.myspaceshooter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;

/**
 * Created by Atte Huhtakangas on 20.1.2015 14:44.
 * -
 * Part of myspaceshooter in package fi.tamk.tiko.myspaceshooter.
 */
public abstract class GameObject implements Disposable {
    protected int viewportWidth;
    protected int viewportHeight;
    protected TextureRegion reg;
    protected Rectangle rectangle;
    protected Vector2 scale;

    private boolean alive;

    public GameObject(TextureRegion reg, int viewportWidth, int viewportHeight) {
        this.reg = new TextureRegion(reg);
        this.viewportWidth = viewportWidth;
        this.viewportHeight = viewportHeight;
        rectangle = setRectangle();
        scale = new Vector2(1, 1);

        init();
    }

    public void init() {
        alive = true;
    }

    public abstract String getTag();

    public boolean collidesWith(GameObject object) {
        if (rectangle.overlaps(object.getRectangle()))
            return true;
        return false;
    }

    protected Rectangle setRectangle() {
        return new Rectangle(0, 0, reg.getRegionWidth(), reg.getRegionHeight());
    }

    abstract void draw(SpriteBatch batch);

    abstract void move(float deltaTime);

    @Override
    public void dispose() {
        Gdx.app.debug(getTag(), "disposed");
    }

    public boolean isAlive() {
        return alive;
    }

    public void die() {
        alive = false;
        Gdx.app.log(getTag(), "died");
    }

    public Rectangle getRectangle() {
        return rectangle;
    }
}
