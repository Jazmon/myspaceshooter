package fi.tamk.tiko.myspaceshooter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Disposable;

/**
 * Created by Atte Huhtakangas on 21.1.2015 23:15.
 * -
 * Part of myspaceshooter in package fi.tamk.tiko.myspaceshooter.
 */
public class AssetLoader implements Disposable, AssetErrorListener {
    public static final String TAG = AssetLoader.class.getName();
    // Singleton!
    public static final AssetLoader instance = new AssetLoader();
    private AssetManager assetManager;

    public AssetSounds sounds;
    public AssetMusic music;
    public AssetSpaceship spaceship;
    public AssetAlien alien;
    public AssetLevelDecoration levelDecoration;
    public AssetBullet bullet;

    // Singleton constructor
    private AssetLoader() {}

    public void init(AssetManager assetManager) {
        this.assetManager = assetManager;
        assetManager.setErrorListener(this);

        // Load texture atlas
        assetManager.load(Constants.TEXTURE_ATLAS, TextureAtlas.class);
        // load other shit
        assetManager.load("effects/exhaust3.p", ParticleEffect.class);
        assetManager.load("background2.jpg", Texture.class);
        assetManager.load("effects/lazor.p", ParticleEffect.class);
        assetManager.load("sounds/Hit_Hurt2.wav", Sound.class);
        assetManager.load("sounds/Laser_Shoot.wav", Sound.class);
        assetManager.load("sounds/Explosion11.wav", Sound.class);
        assetManager.load("music/music3.ogg", Music.class);

        // Start loading assets and wait until finished
        assetManager.finishLoading();

        Gdx.app.debug(TAG, "# of assets loaded: " + assetManager.getAssetNames().size);
        for (String string : assetManager.getAssetNames())
            Gdx.app.debug(TAG, "asset: " + string);

        TextureAtlas atlas = assetManager.get(Constants.TEXTURE_ATLAS);

        // Enable texture filtering
        for (Texture t : atlas.getTextures())
            t.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        // Create game resource objects
        alien = new AssetAlien(atlas);
        spaceship = new AssetSpaceship(atlas, assetManager);
        levelDecoration = new AssetLevelDecoration(assetManager);
        bullet = new AssetBullet(atlas, assetManager);
        sounds = new AssetSounds(assetManager);
        music = new AssetMusic(assetManager);

    }

    /**
     * If asset cannot be found we throw an exception.
     *
     * @param asset     the asset that was not found
     * @param throwable the error that will be passed on
     */
    public void error(AssetDescriptor asset, Throwable throwable) {
        Gdx.app.error(TAG, "Couldn't load asset '" + asset.fileName + "'", throwable);
    }

    /**
     * Disposes the assets
     */
    @Override
    public void dispose() {
        assetManager.dispose();
        Gdx.app.log(TAG, "disposed");
    }

    public class AssetSpaceship {
        public final TextureAtlas.AtlasRegion spaceship;
        public final ParticleEffect exhaust;

        public AssetSpaceship(TextureAtlas atlas, AssetManager am) {
            spaceship = atlas.findRegion("spaceship");
            exhaust = am.get("effects/exhaust3.p", ParticleEffect.class);
        }
    }

    public class AssetAlien {
        public final TextureAtlas.AtlasRegion alien;

        public AssetAlien(TextureAtlas atlas) {
            alien = atlas.findRegion("alienship");
        }
    }

    public class AssetLevelDecoration {
        public final Texture background;

        public AssetLevelDecoration(AssetManager am) {
            background = am.get("background2.jpg", Texture.class);
        }
    }

    public class AssetBullet {
        public final TextureAtlas.AtlasRegion bullet;
        public final ParticleEffect trail;

        public AssetBullet(TextureAtlas atlas, AssetManager am) {
            bullet = atlas.findRegion("bullet");
            trail = am.get("effects/lazor.p", ParticleEffect.class);
        }
    }

    public class AssetSounds {
        public final Sound crash;
        public final Sound laserShot;
        public final Sound explosion;

        public AssetSounds(AssetManager am) {
            crash = am.get("sounds/Hit_Hurt2.wav", Sound.class);
            laserShot = am.get("sounds/Laser_Shoot.wav", Sound.class);
            explosion = am.get("sounds/Explosion11.wav", Sound.class);
        }
    }

    public class AssetMusic {
        public final Music music1;

        public AssetMusic(AssetManager am) {
            // music1 = am.load("music/music3.ogg", Music.class);
            music1 = am.get("music/music3.ogg", Music.class);
        }
    }


}
