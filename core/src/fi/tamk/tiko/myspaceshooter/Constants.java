package fi.tamk.tiko.myspaceshooter;

/**
 * Created by Atte Huhtakangas on 21.1.2015 23:28.
 * -
 * Part of myspaceshooter in package fi.tamk.tiko.myspaceshooter.
 */
public class Constants {
    // Visible game world size
    public static final int VIEWPORT_WIDTH = 1280;
    public static final int VIEWPORT_HEIGHT = 720;
    // location for texture atlas
    public static final String TEXTURE_ATLAS = "textures.atlas";
    // GUI width
    public static final float GUI_WIDTH = 1280;
    // GUI height
    public static final float GUI_HEIGHT = 720;

}
