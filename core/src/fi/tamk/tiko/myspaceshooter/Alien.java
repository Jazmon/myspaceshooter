package fi.tamk.tiko.myspaceshooter;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by Atte Huhtakangas on 20.1.2015 12:19.
 * -
 * Part of myspaceshooter in package fi.tamk.tiko.myspaceshooter.
 */
public class Alien extends GameObject {
    @Override
    public String getTag() {
        return TAG;
    }

    private static final String TAG = Alien.class.getName();
    private float alienSpeedX;
    private float alienSpeedY;

    public Alien(int viewportWidth, int viewportHeight) {
        super(AssetLoader.instance.alien.alien, viewportWidth, viewportHeight);
        alienSpeedX = alienSpeedY = 280.0f * MathUtils.random(0.6f, 2.0f);
    }

    @Override
    protected Rectangle setRectangle() {
        return new Rectangle(MathUtils.random(reg.getRegionWidth(), viewportWidth - reg.getRegionHeight() * 2), MathUtils.random(reg.getRegionWidth(),
                viewportHeight - reg.getRegionHeight()* 2) , reg.getRegionWidth(), reg.getRegionHeight());
    }

    public void draw(SpriteBatch batch) {
        batch.draw(reg.getTexture(),                                        // texture
                rectangle.x, rectangle.y,                                   // x, y
                rectangle.getWidth() / 2, rectangle.getHeight() / 2,        // originX, originY
                rectangle.getWidth(), rectangle.getHeight(),                // width, height
                scale.x, scale.y,                                           // scaleX, scaleY
                0,                                                          // rotation
                reg.getRegionX(), reg.getRegionY(),                         // srcX, srcY
                reg.getRegionWidth(), reg.getRegionHeight(),                // srcWidth, srcHeight
                false, false                                                // flipX, flipY
        );
    }

    public void move(float deltaTime) {
        // check collision
        if (rectangle.x + rectangle.width >= viewportWidth) {
            alienSpeedX = -alienSpeedX;
        } else if (rectangle.x <= 0) {
            alienSpeedX = -alienSpeedX;
        }
        if (rectangle.y + rectangle.height >= viewportHeight) {
            alienSpeedY = -alienSpeedY;
        } else if (rectangle.y <= 0) {
            alienSpeedY = -alienSpeedY;
        }

        // move
        rectangle.x += alienSpeedX * deltaTime;
        rectangle.y += alienSpeedY * deltaTime;
    }
}
